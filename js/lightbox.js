/**
 * Module Lightbox pour creer et afficher une lightbox.
 */

let tab_vignette, id_vignette, photo, url_server = 'https://webetu.iutnc.univ-lorraine.fr';

let init = function (idvignette, src, images){
    let vignette = images[parseInt(idvignette)];
    displayLightbox(vignette, src, images, idvignette);
};

/**
 * Methode pour afficher une lightbox correspondant a une vignette
 */
let displayLightbox = function (vignette, src, tab, id) {
    $('#lightbox_title').text(vignette.photo.titre);
    $('#lightbox_full_img').attr('src', src);
    $('#lightbox_container').css('display', 'block');

    photo = vignette;
    tab_vignette = tab;
    id_vignette = id;
};

/**
 * Methode pour fermer une lightbox
 */
let hideLightbox = function () {
    $('#lightbox_container').css('display', 'none');
    clearData();
};

/**
 * Methode pour passer a la photo suivante
 */
let next = function () {
    if (parseInt(id_vignette)+1 <= 11) {
        displayLightbox(tab_vignette[parseInt(id_vignette)+1], url_server+tab_vignette[parseInt(id_vignette)+1].photo.original.href, tab_vignette, parseInt(id_vignette)+1);
    } else {
        console.log('Veuillez passer a la page suivante');
    }
    clearData();
};

/**
 * Methode pour passer a la photo precedent
 */
let prev = function () {
    if (parseInt(id_vignette)-1 >= 0) {
        displayLightbox(tab_vignette[parseInt(id_vignette)-1], url_server+tab_vignette[parseInt(id_vignette)-1].photo.original.href, tab_vignette,parseInt(id_vignette)-1);
    } else {
        console.log('Veuillez passer a la page precedente');
    }
    clearData();
};

/**
 * Methode pour afficher les informations detailles concernant la photo courante
 */
let loadData = function () {
    clearData();
    let infoPicture = $('<div id="info"></div>');
    infoPicture.append($('<p>Id : '+ photo.photo.id+'</p>'));
    infoPicture.append($('<p>Titre : '+ photo.photo.titre+'</p>'));
    infoPicture.append($('<p>File : '+ photo.photo.file+'</p>'));
    $('#lightbox-img-data').append(infoPicture);
};

/**
 * Methode pour effacer les informations detailles concernant la photo courante
 */
let clearData = function () {
    $('#info').remove();
};

export default {
    init: init,
    displayLightbox: displayLightbox,
    hideLightbox: hideLightbox,
    next: next,
    prev: prev,
    loadData: loadData
}