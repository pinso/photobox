/**
 * Module Photoloader pour gerer les requetes vers l'api.
 */

let server_url; //https://webetu.iutnc.univ-lorraine.fr

/**
 * Methode d'initialisation
 * @param url
 */
let init = function (url) {
    server_url = url;
};

/**
 * Methode de chargement d'une liste d'objets
 * @param uri
 * @returns pr
 */
let loadingPictures = function (uri) {
    let pr = axios.get(server_url+uri,{
        withCredentials:true
    });
    pr.catch(function () {
        console.log('Erreur lors du chargement des images');
    });
    console.log(server_url+uri);
    return pr;
};

export default {
    init: init,
    loadingPictures: loadingPictures
}