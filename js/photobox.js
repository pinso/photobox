/**
 * Module Photobox de l'application qui, lorsque le DOM est charge, realise les differentes initialisation
 */

import photoloader from "./photoloader.js";
import gallery from "./gallery.js";
import lightbox from "./lightbox.js";

$(document).ready(function(){
    photoloader.init('https://webetu.iutnc.univ-lorraine.fr');
    gallery.init('https://webetu.iutnc.univ-lorraine.fr');
});


$('#load_gallery').click(function () {
    gallery.loadingData('/www/canals5/photobox/photos/?offset=0&size=12');
});

$('#lightbox_close').click(function () {
    lightbox.hideLightbox();
});

$('#next').click(function () {
    gallery.nextGallery();
});

$('#previous').click(function () {
    gallery.previousGallery();
});

$('#lightbox-img-next').click(function () {
    lightbox.next();
});

$('#lightbox-img-prev').click(function () {
    lightbox.prev();
});

$('#lightbox-img-bouton').click(function () {
    console.log('Informations');
    lightbox.loadData();
});

