/**
 * Module Gallery pour charger et construire une galerie de photos.
 */

import photoloader from "./photoloader.js";
import lightbox from "./lightbox.js";

let server_url;
let server_url_next;
let server_url_previous;

/**
 * Methode d'initialisation
 */
let init = function (url) {
    server_url = url;
};

/**
 * Methode de chargement des donnees
 * @param uri
 */
let loadingData = function (uri) {
    let pr = photoloader.loadingPictures(uri);
    pr.then(insertGallery);
};

/**
 * Methode d'insertion de la galerie dans le dom
 */
let insertGallery = function (response) {
    $('#photobox-gallery').empty();
    let i = 0;
    response.data.photos.forEach(function (photo) {
        let href = server_url+'/'+photo.photo.original.href;
        let link = server_url+'/'+photo.photo.self;
        let thumbnail = server_url+'/'+photo.photo.thumbnail.href;
        let title = photo.photo.titre;
        let divGen=$('#photobox-gallery');
        let vignette=$('<div class="vignette">\n' +
            '            <img\n' +
            '                    data-img='+href+'\n' +
            '                    data-uri='+link+'\n' +
            '                    src='+thumbnail+'>\n' +
            '            <div class="container">'+title+'</div>\n' +
            '\n' +
            '        </div>');
        divGen.append(vignette);
        i++;
    });

    $('div.vignette').click(function () {
        let titre = this.children[1].innerHTML;
        let src = this.children[0].attributes[0].value;
        let id = titre.slice(titre.indexOf(' '));
        let tab = response.data.photos;
        lightbox.init(id, src,  tab,);
    });

    server_url_next = response.data.links.next.href;
    server_url_previous = response.data.links.prev.href;
};

/**
 * Methode pour charger et afficher la page suivante de la gallerie
 */
function nextGallery() {
    photoloader.loadingPictures(server_url_next).then(insertGallery);
}

/**
 * Methode pour charger et afficher la page precedente de la gallerie
 */
function previousGallery() {
    photoloader.loadingPictures(server_url_previous).then(insertGallery);
}

export default {
    init: init,
    loadingData: loadingData,
    nextGallery: nextGallery,
    previousGallery: previousGallery
}