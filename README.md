# Projet Photobox#

Photobox est une application de parcours et de visualisation de photos, permettant d'afficher et de faire défiler des galeries de photos paginées. Un mode "lightbox", où chaque photo occupe l'ensemble de la fenêtre navigateur est également disponible et permet de parcourir les galeries page par page.

### Fonctionnalités effectuées: ###

```
Partie 1
```

* Exercice 1 : Afficher 1 liste de photos
* Exercice 2 : Lightbox : Affichage d'une photo
* Exercice 3 : Naviguer dans les galeries

```
Partie 2
```

* Exercice 1 : Naviguer dans la lightbox
* Exercice 2 : Affichage des informations détaillées sur une photo dans la lightbox (Id, titre, fichier)

### Liens webetu: ###

https://webetu.iutnc.univ-lorraine.fr/www/pisano5u/photobox/